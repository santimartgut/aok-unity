﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    /**
     * Carga la pantalla de seleccion de personaje y mapa
     */
    public void SinglePlayerButtonPressed()
    {
        SceneManager.LoadScene("RaceSelection");
    }

    /**
     * Cierra el juego
     */
    public void ExitButtonPressed()
    {
        Debug.Log("ExitButton pressed! Exit app only in deploy");
        Application.Quit();
    }

    public void Update()
    {
        if (Input.GetButtonDown("Submit"))
        {
            SinglePlayerButtonPressed();
        }

        if (Input.GetButtonDown("Cancel"))
        {
            ExitButtonPressed();
        }
    }
}
