﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

enum Character
{
    Pirate = 0,
    Viking,
    Chinese,
    Gladiator,
    LENGTH
}

enum Map
{
    Island = 0,
    Colosseum,
    LENGTH
}

public class LobbyMenu : MonoBehaviour
{
    // Elementos para cambiar las imagenes a mostrar
    [SerializeField] private Image mapSelector;
    [SerializeField] private Image characterSelector;

    private Character characterSelected;
    private Map mapSelected;

    private void Start()
    {
        characterSelected = Character.Pirate;
        mapSelected = Map.Island;
    }

    public void Update()
    {
        if (Input.GetButtonDown("Submit"))
        {
            onStartRaceButtonPressed();
        }

        if (Input.GetButtonDown("Cancel"))
        {
            onBackButtonPressed();
        }
    }

    public void onStartRaceButtonPressed()
    {
        //todo: hay que guardar el personaje y mapa y cargarlos en la siguiente escena
        SceneManager.LoadScene("SampleScene");
    }

    public void onBackButtonPressed()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void onChangeMapButtonPressed(bool isRight)
    {
        if (isRight)
        {
            mapSelected++; // Aumenta el contador de mapa

            // Comprueba si hay que reiniciar el contador de mapa
            if ((int)mapSelected >= (int)Map.LENGTH)
            {
                mapSelected = 0;
            }
        }
        else
        {
            mapSelected--; // Disminuye el contador

            // Comprueba si hay que reiniciar el contador de mapa
            if (mapSelected < 0)
            {
                mapSelected = Map.LENGTH - 1;
            }
        }

        //Cambiar la imagen a la que toque
        onChangeMap();
        Debug.Log("Selected map: " + mapSelected);
    }

    private void onChangeMap()
    {
        switch (mapSelected)
        {
            case Map.Island:
                mapSelector.sprite = Resources.Load<Sprite>(UIResourceManager.IslandImage);
                break;
            case Map.Colosseum:
                mapSelector.sprite = Resources.Load<Sprite>(UIResourceManager.ColosseumImage);
                break;
        }
    }

    public void onChangeCharacterButtonPressed(bool isRight)
    {
        if (isRight)
        {
            characterSelected++; // Aumenta el contador de mapa

            // Comprueba si hay que reiniciar el contador de mapa
            if ((int)characterSelected >= (int)Character.LENGTH)
            {
                characterSelected = 0;
            }
        }
        else
        {
            characterSelected--; // Disminuye el contador

            // Comprueba si hay que reiniciar el contador de mapa
            if (characterSelected < 0)
            {
                characterSelected = Character.LENGTH - 1;
            }
        }

        //Cambiar la imagen a la que toque
        onChangeCharacter();
        Debug.Log("Selected char: " + characterSelected);
    }

    private void onChangeCharacter()
    {
        switch (characterSelected)
        {
            case Character.Pirate:
                characterSelector.sprite = Resources.Load<Sprite>(UIResourceManager.PirateImage);
                break;
            case Character.Viking:
                characterSelector.sprite = Resources.Load<Sprite>(UIResourceManager.VikingImage);
                break;
            case Character.Chinese:
                characterSelector.sprite = Resources.Load<Sprite>(UIResourceManager.ChineseImage);
                break;
            case Character.Gladiator:
                characterSelector.sprite = Resources.Load<Sprite>(UIResourceManager.GladiatorImage);
                break;
        }
    }
}
