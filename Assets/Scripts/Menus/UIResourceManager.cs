﻿using UnityEngine;

public class UIResourceManager : MonoBehaviour
{
    // Miniaturas de los personajes disponibles
    public static string PirateImage = "Textures/Menu/Lobby/offline/Characters/PirataOffline";
    public static string VikingImage = "Textures/Menu/Lobby/offline/Characters/VikOffline";
    public static string GladiatorImage = "Textures/Menu/Lobby/offline/Characters/GladiOffline";
    public static string ChineseImage = "Textures/Menu/Lobby/offline/Characters/ChinoOffline";

    // Miniaturas de los mapas disponibles
    public static string IslandImage = "Textures/Menu/Lobby/Maps/mapa_pirata";
    public static string ColosseumImage = "Textures/Menu/Lobby/Maps/mapa_gladiador";
}
