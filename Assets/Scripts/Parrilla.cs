﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parrilla : MonoBehaviour
{
    // Posiciones de la parrilla para localizar los corredores
    [SerializeField] Transform[] Positions = new Transform[6];

    // Start is called before the first frame update
    void Start()
    {
        // Recoger los parametros de la pista para crear los corredores
        // Spawnear los corredores


        // ------- PROVISIONAL ------------
        // Recoger todos los que tengan la etiqueta de corredor
        Corredor[] riders = GameObject.FindObjectsOfType<Corredor>();
        //GameObject[] riders = GameObject.FindGameObjectsWithTag("Corredor"); // No coge ningun elemento

        //Debug.Assert(riders.Length <= 0, "No se ha detectado ningun corredor");

        riders[0].transform.position = Positions[0].position; // Primera posicion unicamente
        riders[0].transform.rotation = Positions[0].rotation;
    }
}
