﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Aceite : Item
{
    [SerializeField] float lifeTime = 100.0f;
    // Start is called before the first frame update

    void Start()
    {
        Rigidbody rgbd = GetComponent<Rigidbody>();
        Destroy(gameObject, lifeTime); // Se autodestruye al cabo del tiempo si no colisiona
    }


    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject); // Se destruye si colisiona
    }
}
