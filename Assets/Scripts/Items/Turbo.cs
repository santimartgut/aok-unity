﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turbo : Item
{
    [SerializeField] float lifeTime = 3.0f;
    [SerializeField] float speed = 120.0f;
    void Start()
    {

        Destroy(gameObject, lifeTime); // Se autodestruye al cabo del tiempo si no colisiona
    }


    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject); // Se destruye si colisiona
    }
}
