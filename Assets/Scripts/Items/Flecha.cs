﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Flecha : Item
{
    [SerializeField] float lifeTime = 10.0f;
    [SerializeField] float speed = 60.0f;

    void Start()
    {
        Rigidbody rgbd = GetComponent<Rigidbody>();
        rgbd.velocity = transform.forward * speed;

        Destroy(gameObject, lifeTime); // Se autodestruye al cabo del tiempo si no colisiona
    }


    private void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject); // Se destruye si colisiona
    }
}
