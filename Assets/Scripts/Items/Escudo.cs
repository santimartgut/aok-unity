﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Escudo : Item
{


    [SerializeField] float lifeTime = 10.0f;
    Vector3 pos;

    void Start()
    {
        Debug.Log("creando el escudo");
        gameObject.SetActive(false);
    } 
     
    void Update()
    {
        //posicion x  y z iguales, cambia la Y
        pos.x = gameObject.transform.GetComponentInParent<Corredor>().transform.position.x;
        pos.y = gameObject.transform.GetComponentInParent<Corredor>().transform.position.y + 1.5f;
        pos.z = gameObject.transform.GetComponentInParent<Corredor>().transform.position.z;

        transform.position = pos;
        transform.Rotate(Vector3.up, -75 * Time.deltaTime); //rota en el eje Y continuamente en la posicion del jugador.
    }

   

   

    private void OnTriggerEnter(Collider other)
    {
       // Destroy(gameObject); // Se destruye si colisiona
    }
}
