﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public enum itemType
    {
        aceite = 0,
        flecha,
        turbo,
        escudo,
        LENGTH
    }


    private int id;
    protected Transform space;
    public virtual void setItem()
    {

    }
     public void setPosition(Vector3 pos)
    {
        space.position = pos;
    }
    public Vector3 getPosition()
    {
        return space.position;
    }
    public void setRotation(Quaternion rot)
    {
        space.rotation = rot;
    }
    public Quaternion getRotation()
    {
        return space.rotation;
    }

}
