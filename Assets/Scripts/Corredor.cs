﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corredor : MonoBehaviour
{
    [SerializeField] private Item activeItem; // Item que puede usar el jugador durante la carrera

    [SerializeField] GameObject throwFront; // Punto frontal de lanzamiento del corredor
    [SerializeField] GameObject throwBack; // Punto trasero de lanzamiento del corredor

    private int itemPos; // ¿?
    private bool ActiveShield = false; //Escudo

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            UseObject();
        }

    }
    public void SetObj(Item item, int pos)//pos = 0 Front // pos = 1 Back
    {
        activeItem = item;
        itemPos    = pos;
     
    }

    private void UseObject()
    {
        // Comprueba que tiene un objecto antes de usarlo
        if (activeItem != null)
        {
            switch (itemPos)
            {
                case 0:
                    Instantiate(activeItem, throwFront.transform.position, throwFront.transform.rotation);
                    break;
                case 1:
                    Instantiate(activeItem, throwBack.transform.position, throwBack.transform.rotation);
                    break;
                case 2:
                    /*
                    for (int i = 0; i < transform.childCount; i++)
                    {
                        Debug.Log(transform.GetChild(i).gameObject.name);
                    }
                    
                    Debug.Log(transform.GetChild(0).gameObject.name);
                    */
                    transform.GetChild(0).gameObject.SetActive(true); // es una cutrada, lo se, pero mientras averiguo como se hace ...
                   // transform.GetChild(0).gameObject.GetComponent<Escudo>().BeginShield();
                    ActiveShield = true;
                    break;
                default:
                    break;
            }
          
        }
    }

    private void setSpeed()
    {
       
    }
}
