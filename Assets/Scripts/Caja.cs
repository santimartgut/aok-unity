﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Caja : MonoBehaviour
{
    // Una opcion para hacerlo de manera aleatoria es con un array/lista de posibilidades en esta clase
    [SerializeField] Item itemInside; // En un futuro tendra que ser aleatorio entre las opciones
    int item;
    public enum itemType
    {
        Aceite = 0,
        Flecha,
        Turbo,
        Escudo,
        LENGTH
    }
    void OnTriggerEnter(Collider col)
    {
        if (col == null || col.gameObject.tag != "Corredor") // No hay chocado con un corredor
        {
            return;
        }

        Corredor c = col.gameObject.GetComponentInParent<Corredor>(); // El corredor es el padre del collider
      
        SetItemToPlayer(c);
        Destroy(gameObject); // Destruir la caja
    }

    void SetItemToPlayer(Corredor c)
    {
       // item = Random.Range(0, 3);
        item = 3;
        Debug.Log("me ha tocado : " + item);
        switch (item)
        {
            case 0:
                c.SetObj(itemInside.GetComponentInChildren<Aceite>(),1); // Obtiene el objeto
                Debug.Log("he cogido: " + itemInside.GetComponentInChildren<Aceite>().name);

                break;
            case 1:
                c.SetObj(itemInside.GetComponentInChildren<Flecha>(),0); // Obtiene el objeto
                Debug.Log("he cogido: " + itemInside.GetComponentInChildren<Flecha>().name);

                break;
            case 2:
                c.SetObj(itemInside.GetComponentInChildren<Turbo>(),0); // Obtiene el objeto
                Debug.Log("he cogido: " + itemInside.GetComponentInChildren<Turbo>().name);

                break;
            case 3:
                c.SetObj(itemInside.GetComponentInChildren<Escudo>(),2); // Obtiene el objeto
                Debug.Log("he cogido: " + itemInside.GetComponentInChildren<Escudo>().name);

                break;
            default:
                break;
        }

        

    }
}
